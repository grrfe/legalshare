import argparse
from util import read_bytes

parser = argparse.ArgumentParser(description="Join two files")
parser.add_argument("infile1", help="Path to the first input file")
parser.add_argument("infile2", help="Path to the second input file")
parser.add_argument("outfile", help="Path to the output file")

args = parser.parse_args()

with open(args.outfile, "wb+") as file:
    for byte1, byte2 in zip(read_bytes(args.infile1), read_bytes(args.infile2)):
        file.write(bytes([byte1 - byte2]))
