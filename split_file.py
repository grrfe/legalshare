import random
import argparse
from util import read_bytes

parser = argparse.ArgumentParser(description="File splitter")
parser.add_argument("infile", help="Path to the file that should be splitted")
parser.add_argument("outfile1", help="Path to the first output file")
parser.add_argument("outfile2", help="Path to the second output file")

args = parser.parse_args()

with open(args.outfile1, "wb+") as ofile1, open(args.outfile2, "wb+") as ofile2:
    for byte in read_bytes(args.infile):
        rand_max = 255 - byte
        if rand_max != 0:
            rand_byte = random.randint(1, rand_max)
        else:
            rand_byte = 0

        new_byte = byte + rand_byte

        ofile1.write(bytes([new_byte]))
        ofile2.write(bytes([rand_byte]))
