def read_bytes(file, size=8192):
    with open(file, "rb") as f:
        while True:
            chunk = f.read(size)
            if chunk:
                for b in chunk:
                    yield b
            else:
                break