# legalshare

This project allows you to legally share copyright protected material using the [split_file script](split_file.py)

***How it works***

* Read every byte `b` of the input file
* For every byte
    * Calculate `rand_max`: `255` - `b` (since bytes are in range 0 (inclusive) - 256 (exclusive))
    * If `rand_max` > `0`
        * Generate a random byte `rand_byte` in the range `1` to `rand_max`
    * If `rand_max` = `0`
        * `rb` = `0`
    * Calculate new byte `new_byte`: `rand_byte` + `b`
    * Write 
        * `new_byte` to `file1` 
        * `rand_byte` to `file2`


***Example***

* File contains the following bytes: `[33, 129, 255]` 
* The corresponding `rand_max` bytes would look like this: `[222, 126, 0]`
* The generated `random bytes` might look like this: `[40, 75, 0]`
* From that, the calculated `new bytes` look like this: `[73, 204, 255]`
* Our `file1` now looks like `new bytes`: `[73, 204, 255]`
* While our `file2` looks like `random bytes`: `[40, 75, 0]`
* The only thing they have in common with the original file `[33, 129, 255]` is `[x, x, 255]` which is only the case because the last byte equals `255`


Share `file1` and `file2` separately; This should *theoretically* be legal since you are sharing two files which contain random bytes which make no sense until you combine them. 

Using the [join_files script](join_files.py) the files will be combined and readable again. 

***Disadvantages***

`file1` and `file2` will both be as big as the input file
